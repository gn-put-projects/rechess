/*
  file: src/movement.h
  purpose: define structs for describing piece movement
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MOVEMENT_H
#define MOVEMENT_H

struct Condition_S {
  int x;
  int y;
  string body;
};
typedef struct Condition_S Condition;

struct Action_S {
  int x;
  int y;
  string body;
};
typedef struct Action_S Action;

struct Special_movement_S {
  std::vector<Condition> conditions;
  std::vector<Action> actions;
};
typedef Special_movement_S Special_move;

struct Movement{
  int knight;
  int forward;
  int backward;
  int left;
  int right;
  int topleft;
  int topright;
  int botleft;
  int botright;
};
typedef struct Movement Movement;

struct Move{
  int x;
  int y;
  Special_move* special;
};
typedef struct Move Move;

#endif
