/*
  file: src/chess_rules.h
  purpose: define movement constants describing the original
           game of chess
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHESS_RULES_H
#define CHESS_RULES_H

#include <vector>

#include "movement.h"

#define inf INT_MAX

using std::vector;

namespace chess_rules{
  const Movement king_movement
  {0,1,1,1,1,1,1,1,1};
  
  const Movement rook_movement
  {0,inf,inf,inf,inf,0,0,0,0};
  
  const Movement knight_movement
  {1,0,0,0,0,0,0,0,0};
  
  const Movement bishop_movement
  {0,0,0,0,0,inf,inf,inf,inf};
  
  const Movement queen_movement
  {0,inf,inf,inf,inf,inf,inf,inf,inf};
  
  const Movement pawn_movement
  {0,0,0,0,0,0,0,0,0};
  
  const Special_move wpawn_move {
    {
      Condition{0, 1, "piece none"}
    },
    {
      Action{0, 0, "move 0 1"}
    }
  };

  const Special_move wpawn_enpassant_l {
    {
      Condition{-1, 0, "piece pawn"},
      Condition{-1, 0, "moved = 2"}
    },
    {
      Action{-1, 0, "remove"},
      Action{0, 0, "move -1 1"}
    }
  };

  const Special_move wpawn_enpassant_r {
    {
      Condition{1, 0, "piece pawn"},
      Condition{1, 0, "moved = 2"}
    },
    {
      Action{1, 0, "remove"},
      Action{0, 0, "move 1 1"}
    }
  };

  const Special_move bpawn_enpassant_l {
    {
      Condition{-1, 0, "piece pawn"},
      Condition{-1, 0, "moved = 2"}
    },
    {
      Action{-1, 0, "remove"},
      Action{0, 0, "move -1 -1"}
    }
  };

  const Special_move bpawn_enpassant_r {
    {
      Condition{1, 0, "piece pawn"},
      Condition{1, 0, "moved = 2"}
    },
    {
      Action{1, 0, "remove"},
      Action{0, 0, "move 1 -1"}
    }
  };
  
  const Special_move bpawn_move {
    {
      Condition{0, -1, "piece none"}
    },
    {
      Action{0, 0, "move 0 -1"}
    }
  };

  const Special_move bpawn_attack_l {
    { Condition{-1, -1, "enemy"} },
    { Action{0, 0, "move -1 -1"} }
  };
  const Special_move bpawn_attack_r {
    { Condition{1, -1, "enemy"} },
    { Action{0, 0, "move 1 -1"} }
  };

  const Special_move wpawn_attack_l {
    { Condition{-1, 1, "enemy"} },
    { Action{0, 0, "move -1 1"} }
  };
  
  const Special_move wpawn_attack_r {
    { Condition{1, 1, "enemy"} },
    { Action{0, 0, "move 1 1"} }
  };

  const Special_move double_wpawn_move {
    {
      Condition{0, 1, "piece none"},
	Condition{0, 2, "piece none"},
	Condition{0, 0, "played = 0"}
    },
      {
	Action{0, 0, "move 0 2"}
      }
  };

  const Special_move double_bpawn_move {
    {
      Condition{0, -1, "piece none"},
	Condition{0, -2, "piece none"},
	Condition{0, 0, "played = 0"}
    },
      {
	Action{0, 0, "move 0 -2"}
      }
  };
  
  const Special_move castling_queenside {
    {
      Condition{0, 0, "attacked 0"},
	Condition{0, 0, "played = 0"},
	Condition{-1, 0, "piece none"},
	Condition{-1, 0, "attacked 0"},
	Condition{-2, 0, "piece none"},
	Condition{-2, 0, "attacked 0"},
	Condition{-3, 0, "piece none"},
	Condition{-4, 0, "piece rook"},
	},
      {
	Action{0, 0, "move -2 0"},
	  Action{-2, 0, "move 1 0"}
      }
  };
  
  const Special_move castling_kingside {
    {
      Condition{0, 0, "attacked 0"},
      Condition{0, 0, "played = 0"},
      Condition{1, 0, "piece none"},
      Condition{1, 0, "attacked 0"},
      Condition{2, 0, "piece none"},
      Condition{2, 0, "attacked 0"},
      Condition{3, 0, "piece rook"},
    },
    {
      Action{0, 0, "move 2 0"},
      Action{1, 0, "move -1 0"}
    }
  };

  const vector<Special_move> black_pawn = {
    bpawn_move,
    double_bpawn_move,
    bpawn_attack_l,
    bpawn_attack_r,
    bpawn_enpassant_l,
    bpawn_enpassant_r
  };

  const vector<Special_move> white_pawn = {
    wpawn_move,
    double_wpawn_move,
    wpawn_attack_l,
    wpawn_attack_r,
    wpawn_enpassant_l,
    wpawn_enpassant_r
  };
}

#endif
