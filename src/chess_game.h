/*
  file: src/game.h
  purpose: declare the ChessGame class
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHESS_GAME_H
#define CHESS_GAME_H

#include <iostream>
#include <ctime>
#include <cmath>
#include <string>

// Forward declaration
class ChessGame;

#include "piece.h"
#include "board.h"
#include "helpers.h"
#include "interface.h"
#include "movement.h"
#include "sfml_frontend/sfml_chess_game.h"

using std::vector;
using std::string;

enum keycodes{
 ENTER=10
};
  
class ChessGame{
private:
  vector<Piece*> pieces;
  Interface* interface;
  void user_input();
  void remove_all_pieces();
  void add_piece(char, int, int);
  Piece* selectedPiece;
public:
  Board* board;
  void remove_piece(int, int);
  void update_moves(bool);
  void loop();
  void load_from_fen(string);
  string save_to_fen();
  ChessGame(Interface*);
  ~ChessGame();
  void print_debug_info();
  void clear_last_moves();
  sf::Vector2i find_piece(char);
  friend SfmlChessGame;
};

#endif
