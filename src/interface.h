#ifndef INTERFACE_H
#define INTERFACE_H

#include <iostream>
#include <cmath>
#include <ctime>
#include <string>
#include <filesystem>
#include <unistd.h>
#include <termios.h>

// Foraward declaration
class Interface;

#include "helpers.h"
#include "board.h"
#include "piece.h"

#define VERSION "1.0"
#define YEARS 2022

using std::string;
namespace fs = std::filesystem;

class Interface{
private:
  struct termios default_termios;
  Board* board;
  Piece* piece;
public:
  int process_flags(int, char**);
  void canonical_mode(bool);
  void loop();
  Interface();
  ~Interface();
};

#endif
