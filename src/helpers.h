#ifndef HELPERS_H
#define HELPERS_H

#include <iostream>
#include <ctime>
#include <cmath>
#include <string>
#include <filesystem>
#include <unistd.h>

#define STDIN 0

using std::string;

void clear();
int int_length(int);
string repeat(const string&, int);
double rand_double(double, double);
void colors(int, int);
void restore_colors();
int sign(int);
void get_two_ints_from_string(string, int*, int*);
#endif
