/*
  file: src/board.h
  purpose: declare the board class
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <vector>
#include <map>

// Forward declaration
enum {row=0, column=1};
class Board;

#include "helpers.h"
#include "piece.h"
#include "terminal_colors.h"
#include "position.h"
#include "movement.h"
#include "chess_game.h"

using std::vector;
using std::string;

class Board{
public:
  Board(ChessGame*,int,int);
  int x;
  int y;
  int size;
  /**
     A 2d structure representing the board.
     board[0][0] represents square a1 (bottom left corner for white).
  */
  vector<vector<bool> > highlighted;
  vector<vector<char> > enumerated;
  vector<vector<Piece*> > board;
  vector<vector<bool> > attackedByW;
  vector<vector<bool> > attackedByB;
  ChessGame* game;
  void print();
  ~Board();
  void move(Piece*, int, int);
  void remove(Piece*);
  void place(Piece*, int, int);
  void clear_last_moves();
  void select(int, int);
  void clear_selection();
  int selection_x;
  int selection_y;
  bool isEmpty(int, int);
  bool isEnemy(int, int, Piece*);
  void set_highlights(vector<Move>&);
  void clear_highlights();
  void clear_attacks();
  void remove_all_pieces();
  std::map<char, Move*> enumerator;
  bool is_piece(int, int, string);
  bool check_condition(Condition, Piece*);
  void manage_action(Action, Piece*);
  bool isLegal(Piece*, Move*);
};

#endif
