#include "helpers.h"

void clear() {
  const char *CLEAR_SCREEN_ANSI = "\e[1;1H\e[2J\0";
  write(STDOUT_FILENO, CLEAR_SCREEN_ANSI, 12);
}

int int_length(int n){
  if (!n) return 1;
  int add = 1;
  if (n<0) {
    n=-n;
    add++;
  }
  return trunc(log10(n)) + add;
}

string repeat(const string& s, int times){
  string output = "";
  output.reserve(s.size() * times);
  while (times --> 0)
    output += s;
  return output;
}

double rand_double(double min, double max){
  return min + (double)(rand()) / ((double)(RAND_MAX/(max-min)));
}

void colors(int fg, int bg){
  std::cout<<"\x1B["<<bg+10<<";"<<fg<<"m";
}

void restore_colors(){
  std::cout<<"\x1B[0;0m";
}

int sign(int x){
  if (x<0) return -1;
  return 1;
}

void get_two_ints_from_string(string s, int* x, int* y){
  for (int i=0; i<(int)s.length();++i){
    if (s[i] == ' '){
      *x = stoi(s.substr(0, i));
      *y = stoi(s.substr(i+1));
      return;
    }
  }
  throw std::invalid_argument
    ("Provided string does not contain two space-separated integers"); 
}

