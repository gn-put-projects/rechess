#include "sfml_chess_game.h"
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Vector2.hpp>

SfmlChessGame::SfmlChessGame (sf::RenderWindow* rw, Config* config, ChessGame* game){
  this->windowP = rw;
  this->config = config;
  this->game = game;
  this->turn = "white";
  this->state = "move";
  this->game->load_from_fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR ");
  //this->game->load_from_fen("r1bqkbnr/pPpppppp/8/8/8/8/PpPPPPPP/R1BQK2R ");
  //this->game->load_from_fen("3k4/pPpppppp/8/8/8/8/PpPPPPPP/R1BQK2R ");
  //this->board = new Board(this,8,8);
  //this->pieces.clear();
  //this->selectedPiece = nullptr;
}

void SfmlChessGame::moveSelectedPiece(Move* move){
  this->turn = (this->turn == "white")? "black" : "white";
  this->game->selectedPiece->move(move);
  int ymax = (this->game->board->y)-1;
  if ((this->game->selectedPiece->fenchar == 'P' &&
       this->game->selectedPiece->y == ymax) ||
      (this->game->selectedPiece->fenchar == 'p' &&
       this->game->selectedPiece->y == 0)){
    this->state = "promote";
    this->promoting = this->game->selectedPiece;
    this->promo_options = this->promoting->get_promotion_options();
  }
  this->game->selectedPiece = nullptr;
  this->game->board->clear_selection();
  this->game->board->clear_highlights();
}

bool SfmlChessGame::manageEvent(sf::Event event){
#define brd this->game->board
  if (event.type == sf::Event::MouseButtonPressed){
    auto [cx,cy] = sf::Mouse::getPosition(*this->windowP);
#define conf this->config->board
    float width = conf.size.x;
    float height = conf.size.y;
    float posX = conf.position.x;
    float posY = conf.position.y;
    int x = this->game->board->x;
    int y = this->game->board->y;
    float square_width = width / x;
    float square_height = height / y;
    float lowY = posY + height - square_height;
    float lowX = posX;

    if (!this->state.compare("promote")){
      int promox = promo_options.size();
      float box_x = lowX + width/2 - square_width*promox/2;
      float box_y = lowY - height/2 + square_height/2;
      cx -= box_x;
      cy = box_y+square_height-cy;
      if (cx >= 0 && cy >= 0 && cx <= promox*square_width
	  && cy <= square_height){
	int dest_x = cx/square_width;
	if ((int)this->promo_options.size() > dest_x && dest_x > -1){
	  this->promoting->promote(this->promo_options[dest_x]);
	  this->state = "move";
	  this->promoting = nullptr;
	  this->promo_options.clear();
	}
	return true;
      }
      return false;
    }
    cx -= posX;
    cy = posY+height-cy;

    if (cx >= 0 && cy >= 0 && cx <= width && cy <= height){
      int dest_x = cx/square_width;
      int dest_y = cy/square_height;
      Piece* sp = this->game->selectedPiece;
      if (sp && !sp->color.compare(this->turn)){
	Move potential_move = {dest_x, dest_y};
	Move* move_to_make;
	//std::cout<<"pot "<<potential_move.x<< " "<<potential_move.y<<"\n";
	if ((move_to_make = sp->get_move(potential_move))){
	  this->moveSelectedPiece(move_to_make);
	  return true;
	}
      }
      this->game->selectedPiece = brd->board[dest_x][dest_y];
      brd->clear_selection();
      brd->clear_highlights();
      return true;
    }
    return false;
  }
  int input = event.key.code;
  if (!this->state.compare("promote")){
    if ((int)this->promo_options.size() > input && input > -1){
      this->promoting->promote(this->promo_options[input]);
      this->state = "move";
      this->promoting = nullptr;
      this->promo_options.clear();
    }
    return true;
  }
  if (brd->enumerator.find(input+'a') != brd->enumerator.end()){
    this->moveSelectedPiece(brd->enumerator[input+'a']);
    return true;
  }
  if (input >= sf::Keyboard::Num1 && input <= sf::Keyboard::Num8){
    this->game->selectedPiece = nullptr;
    brd->enumerator.clear();
    
    brd->select(row, input - sf::Keyboard::Num1);
    brd->clear_highlights();
    return true;
  }
  if (input >= sf::Keyboard::A && input <= sf::Keyboard::H){
    this->game->selectedPiece = nullptr;
    brd->enumerator.clear();
    
    brd->select(column, input - sf::Keyboard::A);
    brd->clear_highlights();
    return true;
  }
  switch (input){
  case sf::Keyboard::N:
    // tell the game engine that we need to swap stages
    this->status = 1;
    return false;
    break;
  case sf::Keyboard::Enter:
    if (brd->selection_x+1 && brd->selection_y+1 &&
	brd->board[brd->selection_x][brd->selection_y] &&
	brd->board[brd->selection_x][brd->selection_y]->color
	.compare(this->turn) == 0){
      this->game->selectedPiece =
	brd->board[brd->selection_x][brd->selection_y];
      brd->clear_selection();
      brd->clear_highlights();
      return true;
    }
    break;
  default: break;
  }
  brd->clear_selection();
  return false;
#undef brd
}

void SfmlChessGame::update() {
  this->game->update_moves(true);
  if (this->game->selectedPiece)
    this->game->board->set_highlights(this->game->selectedPiece->moves);
}

void SfmlChessGame::render() {
  //std::cout<<"Game redraw!\n";
  #define conf this->config->board
  float width = conf.size.x;
  float height = conf.size.y;
  float posX = conf.position.x;
  float posY = conf.position.y;

  this->windowP->clear(this->config->menu.color);
  
  sf::RectangleShape rect(sf::Vector2f(width, height));
  rect.setPosition(sf::Vector2f(posX, posY));
  rect.setFillColor(conf.bColor);
  this->windowP->draw(rect);

  int x = this->game->board->x;
  int y = this->game->board->y;
    
  float square_width = width / x;
  float square_height = height / y;
  rect.setSize(sf::Vector2f(square_width, square_height));
  rect.setFillColor(conf.wColor);

  float lowY = posY + height - square_height;
  float lowX = posX;
  
  sf::Texture texture;
  texture.loadFromFile("img/figures.png");

  int sw = 100; //source width (of one piece)
  int sh = 100; //source height (of one piece)
  std::map<char, sf::IntRect> spritesheet = {
    {'P', {0, 0, sw, sh}}, {'p', {0, sh, sw, sh}},
    {'B', {sw, 0, sw, sh}}, {'b', {sw, sh, sw, sh}},
    {'K', {2*sw, 0, sw, sh}}, {'k', {2*sw, sh, sw, sh}},
    {'Q', {3*sw, 0, sw, sh}}, {'q', {3*sw, sh, sw, sh}},
    {'N', {4*sw, 0, sw, sh}}, {'n', {4*sw, sh, sw, sh}},
    {'R', {5*sw, 0, sw, sh}}, {'r', {5*sw, sh, sw, sh}}
  };
  sf::Sprite sprite(texture);
  sprite.setScale(square_width/sw, square_height/sh);

  
  for (int j=0;j<x;++j){
    for (int i=(j+1)%2; i<y;i+=2){
      rect.setPosition
	(sf::Vector2f(lowX + square_width*j, lowY - square_height*i));
      this->windowP->draw(rect);
    }
  }
  for (auto piece : this->game->pieces){
    sprite.setTextureRect(spritesheet[piece->fenchar]);
    sprite.setPosition(posX + piece->x*square_width,
		       posY + (y-1-piece->y)*square_height);
    this->windowP->draw(sprite);
  };

  rect.setFillColor(conf.hColor);
  bool x_selected, y_selected;
  sf::Text enumeratorText;
  enumeratorText.setCharacterSize(this->config->board.enumeratorText.size);
  enumeratorText.setFillColor(this->config->board.enumeratorText.color);
  enumeratorText.setFont(this->config->board.enumeratorText.font);
  float textOffsetX = this->config->board.enumeratorText.offsetX;
  float textOffsetY = this->config->board.enumeratorText.offsetY; 
#define board this->game->board
  for (int i=0; i<x; ++i){
    for (int j=0; j<y; ++j){
      x_selected = board->selection_x+1;
      y_selected = board->selection_y+1;
      if (board->highlighted[i][j] ||
	  ((i == board->selection_x || !x_selected) &&
	   (j == board->selection_y || !y_selected) &&
	   (x_selected || y_selected))){
	rect.setPosition
	  (sf::Vector2f(lowX + square_width*i, lowY - square_height*j));
	this->windowP->draw(rect);
      }
      if (board->enumerated[i][j]){
	enumeratorText.setPosition
	  (sf::Vector2f(lowX + square_width*(i+textOffsetX),
			lowY - square_height*(j+textOffsetY)));
	enumeratorText.setString(std::string(1, board->enumerated[i][j]));
	this->windowP->draw(enumeratorText);
      }
    }
  }

  if (!this->state.compare("promote")){
    rect.setFillColor(sf::Color(0, 0, 0, 200));
    rect.setSize(sf::Vector2f(width, height));
    rect.setPosition
	(sf::Vector2f(lowX, posY));
    this->windowP->draw(rect);

    int promox = promo_options.size();
    float box_o = 2;
    float box_x = lowX + width/2 - square_width*promox/2;
    float box_width = promox * square_width;
    float box_y = lowY - height/2 + square_height/2;
    float box_height = 1 * square_height;
    float text_offset = 2;
    float text_offset_y = -2;
    sf::Color outline_color =
      (!this->promoting->color.compare("white"))? sf::Color::White :
      sf::Color::Black;

    sf::Color box_color =
      (!this->promoting->color.compare("black"))? sf::Color::White :
      sf::Color::Black;
      
    rect.setSize(sf::Vector2f(box_width + 2*box_o, box_height + 2*box_o));
    rect.setPosition(sf::Vector2f(box_x-box_o, box_y-box_o));
    
    rect.setFillColor(outline_color);
    this->windowP->draw(rect);

    rect.setSize(sf::Vector2f(box_width, box_height));
    rect.setPosition(sf::Vector2f(box_x, box_y));
    rect.setFillColor(box_color);
    this->windowP->draw(rect);
    
    for(int i=0; i<promox;++i){
      sprite.setTextureRect(spritesheet[promo_options[i]]);
      sprite.setPosition(box_x + i*square_width, box_y);
      this->windowP->draw(sprite);
    
      enumeratorText.setFillColor(sf::Color::White);
      enumeratorText.setCharacterSize(20);
      enumeratorText.setPosition
	(sf::Vector2f(box_x + i*square_width + textOffsetX + text_offset,
		      box_y + textOffsetY - square_height/2 + text_offset_y));
      enumeratorText.setString(std::string(1, (char)(i+'a')));
      this->windowP->draw(enumeratorText);
    }
  }
#undef board
#undef conf
}

SfmlChessGame::~SfmlChessGame(){
}
