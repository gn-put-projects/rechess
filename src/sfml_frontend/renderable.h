#ifndef REND_H
#define REND_H

#include "config.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include "renderable.h"

class Renderable{
/* This class makes graphical interface generation easier
It's responsible for displaying the stage and running interaction functions */    
public:
  void update();
  void render();
  //Renderable(){};
  //virtual ~Renderable(){};
};

#endif
