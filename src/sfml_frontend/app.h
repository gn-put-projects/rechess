#ifndef APP_H
#define APP_H

#include <SFML/Graphics.hpp>

#include "config.h"
#include "stage.h"
#include "sfml_chess_game.h"
#include "renderable.h"

/**
App class serves as the game engine for the graphical interface.
It's responsible for updating the state of the application
as well as rendering graphics to the game window.
*/
class App{
private:
  sf::RenderWindow* windowP;
  // event stores the event that has been or is being processed
  sf::Event event;
  sf::VideoMode windowSize;
  std::string windowName;
  Config config;
  int stage; // current stage indicator
  Stage* menuP;
  SfmlChessGame* chess;
  
  void initConfig();
  void initVariables();
  void initWindow();

  bool manageEvents();
  bool update();
  void render();
public: 
  void loop();
  //Contructor & Destructor
  App();
  virtual ~App();
};

#endif
