#include "app.h"

//PRIVATE

void App::initConfig(){
    if (!this->config.menu.button.text.font.loadFromFile("fonts/WeretigerRegular.ttf")){
      throw std::runtime_error("Error while loading font file\n");
    }
    if (!this->config.menu.title.font.loadFromFile("fonts/WeretigerRegular.ttf")){
      throw std::runtime_error("Error while loading font file\n");
    }
    if (!this->config.board.grid.number.font.loadFromFile("fonts/WeretigerRegular.ttf")){
      throw std::runtime_error("Error while loading the font file\n");
    }
    if (!this->config.board.enumeratorText.font.loadFromFile("fonts/MonospaceBold.ttf")){
      throw std::runtime_error("Error while loading the font file\n");
    }
    this->config.menu.title.size = 72;
    this->config.menu.title.color = sf::Color(255, 255, 255, 255);

    this->config.menu.color = sf::Color(30, 30, 25, 255);
    this->config.menu.button.color = sf::Color(100, 100, 95, 255);
    this->config.menu.button.outline.color = sf::Color(0, 0, 0, 255);
    this->config.menu.button.outline.thickness = 3.f;
    this->config.menu.button.size = sf::Vector2f(250.f,100.f);

    this->config.menu.button.text.size = 26;
    this->config.menu.button.text.color = sf::Color(0, 0, 0, 255);

    this->config.board.wColor = sf::Color(sf::Color(90,90,90, 255));
    this->config.board.bColor = sf::Color(sf::Color(50, 50, 45, 255));
    this->config.board.hColor = sf::Color(sf::Color(0, 200, 0, 100));
    
    this->config.board.size = sf::Vector2f(422.f, 422.f);

    this->config.board.grid.color = sf::Color(205, 205, 205, 255);
    this->config.board.grid.thickness = 1.f;

    this->config.board.grid.zoneColor = sf::Color(255, 255, 255, 255);
    this->config.board.grid.zoneThickness = 2.f;
    
    this->config.board.grid.number.size = 35;
    this->config.board.grid.number.color = sf::Color(255, 255, 255, 255);
    
    this->config.board.cursor.color = sf::Color(60, 60, 115, 255);
    this->config.board.cursor.tail = sf::Color(100, 100, 155, 115);

    this->config.board.position = sf::Vector2f(29.f, 50.f);

    this->config.board.enumeratorText.color = sf::Color(255, 255, 255, 255);
    this->config.board.enumeratorText.size = 15;
    this->config.board.enumeratorText.offsetX = 0;
    this->config.board.enumeratorText.offsetY = 0.05;
}

/**
   Initialize the class's variables.
   Returns true on success and false on failure
 */
void App::initVariables(){
  this->stage = 0;
  this->windowP = nullptr;
  this->menuP = nullptr;
  this->chess = nullptr;
  this->windowSize.width = 480;
  this->windowSize.height = 800;
  this->windowName = "ReChess";
  
  this->initConfig();
}

void App::initWindow(){
    // Has to be called AFTER initVariables()
    this->windowP = new sf::RenderWindow(this->windowSize, this->windowName);
    this->windowP->setFramerateLimit(60);

    // Creating menu objects
    sf::RectangleShape newApp;
    newApp.setSize(this->config.menu.button.size);
    newApp.setFillColor(this->config.menu.button.color);
    newApp.setOutlineColor(this->config.menu.button.outline.color);
    newApp.setOutlineThickness(this->config.menu.button.outline.thickness);
    newApp.setPosition(sf::Vector2f(100.f,150.f));

    sf::RectangleShape* rectangles[] = {&newApp};


    sf::Text gameNameText;
    gameNameText.setFont(this->config.menu.title.font);
    gameNameText.setString("ReChess");
    gameNameText.setCharacterSize(this->config.menu.title.size);
    gameNameText.setFillColor(this->config.menu.title.color);
    gameNameText.setPosition(sf::Vector2f(40.f, 20.f));

    sf::Text newAppText;
    newAppText.setFont(this->config.menu.button.text.font);
    newAppText.setString("Nowa gra");
    newAppText.setCharacterSize(this->config.menu.button.text.size);
    newAppText.setFillColor(this->config.menu.button.text.color);
    newAppText.setPosition(sf::Vector2f(120.f, 170.f));

    sf::Text* texts[] = {&gameNameText, &newAppText};
    this->menuP = new Stage(this->windowP, this->config,
        rectangles, 1, texts, 2);
    this->update();
    this->render();
}

bool App::manageEvents(){
  if (this->windowP->pollEvent(this->event)){
    switch (this->event.type){
    case sf::Event::Closed:
      this->windowP->close();
      return false;
      break;
    case sf::Event::Resized:
    case sf::Event::GainedFocus:
      this->render();
      break;
    case sf::Event::MouseButtonPressed:
    case sf::Event::KeyPressed:
      switch(this->stage){
      case 0:
	return this->menuP->manageEvent(this->event);
	break;
      case 1:
	return this->chess->manageEvent(this->event);
	break;
      default: break;
      }
      break;
    default: break;
    }
  }
  return false;
}

bool App::update(){
  bool need_to_redraw = this->manageEvents();
  if (this->stage == 0 && this->menuP->status == 1){
    this->chess = new SfmlChessGame(this->windowP, &this->config, new ChessGame(nullptr));
    this->stage = 1;
    need_to_redraw = true;
  }
  else if (this->stage == 1 && this->chess->status == 1){
    delete this->chess;
    this->menuP->status = 0;
    this->stage = 0;
    need_to_redraw = true;
  }
  if (!need_to_redraw) return false;
  
  switch(this->stage){
  case 0:
    this->menuP->update();
    break;
  case 1:
    this->chess->update();
    break;
  default:
    printf("Unknown stage!\n");
    break;
  }
  //state->update();
  //state->render();
  return need_to_redraw;
}

void App::render(){
  switch(this->stage){
  case 0:
    this->menuP->render();
    break;
  case 1:
    this->chess->render();
    break;
  default:
    printf("Unknown stage!");
    break;
  }
  this->windowP->display();
}

//PUBLIC
void App::loop(){
  while(this->windowP->isOpen()){
    if(this->update())
      this->render();
  }
}

App::App(){
  //srand(time(NULL));
  this->initVariables();
  this->initWindow();
}

App::~App(){
  //delete this->windowP;
    //delete this->menuP;
    //delete this->chess;
}
