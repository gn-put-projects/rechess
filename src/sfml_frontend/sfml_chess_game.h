#ifndef SFML_CHESS_GAME_H
#define SFML_CHESS_GAME_H

// Forward declaration
class SfmlChessGame;

#include <stdio.h>
#include <string>
#include <map>
#include <SFML/Graphics.hpp>

#include "../chess_game.h"
#include "config.h"
#include "renderable.h"

using std::string;

class SfmlChessGame : Renderable{ 
 private:
  sf::RenderWindow* windowP;
  Config* config;
  ChessGame* game;
  string turn;
  string state;
  Piece* promoting;
  std::vector<char> promo_options;
  void moveSelectedPiece(Move*);
 public:
  int status;
  
  bool manageEvent(sf::Event);

  void update();
  void render();

  SfmlChessGame(sf::RenderWindow*, Config*, ChessGame*);
  ~SfmlChessGame();
};
#endif
