#ifndef STAGE_H
#define STAGE_H

#include "config.h"
#include <SFML/Graphics.hpp>
#include <iostream>
#include "renderable.h"

class Stage : Renderable{
/* This class makes graphical interface generation easier
It's responsible for displaying the stage and running interaction functions */    
private:
    sf::RenderWindow* windowP;
    Config config;
    void initVariables();
    std::vector<sf::RectangleShape> rectangles;
    int rectangleCount;
    std::vector<sf::Text> texts;
    int textCount;
    // status - specifies if and which different stage or other rendering system should be used.
    // 0 indicates that menu should be rendered
    
public:
  int status;
  // General Functions
  bool manageEvent(sf::Event);
  void click();
  void update();
  void render();
    //Contructor & Destructor
    Stage(sf::RenderWindow*, Config,
          sf::RectangleShape* [], int,
          sf::Text* [], int);
    virtual ~Stage();
};

#endif
