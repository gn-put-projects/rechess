#include "stage.h"

//PRIVATE
void Stage::initVariables(){
    this->windowP = nullptr;
    this->rectangleCount = 0;
    this->textCount = 0;
    this->status = 0;
}

//PUBLIC
void Stage::update(){
}

bool Stage::manageEvent(sf::Event event){
  if (event.type == sf::Event::MouseButtonPressed){
    auto [cx,cy] = sf::Mouse::getPosition(*this->windowP);
    auto [bx,by] = this->rectangles[0].getPosition();
    auto [bw,bh] = this->rectangles[0].getSize();
    
    if (cx >= bx && cx <= bx+bw &&
	cy >= by && cy <= by+bh){
      this->status = 1;
      return true;
    }
    return false;
  }

  switch (event.key.code){
  case sf::Keyboard::N:
    // tell the game engine that we need to swap stages
    this->status = 1;
    return true;
    break;
  default: break;
  }
  return false;
}

void Stage::render(){
  //printf("Stage redraw!\n");
  this->windowP->clear(this->config.menu.color);
  for (int i=0;i < this->rectangleCount;++i)
    this->windowP->draw(this->rectangles[i]);
  for (int i=0;i < this->textCount;++i)
    this->windowP->draw(this->texts[i]);
}

Stage::Stage(sf::RenderWindow* windowPointer, Config config,
             sf::RectangleShape* rectanglePointers[], int rCount,
             sf::Text* textPointers[], int tCount){
    this->initVariables();
    this->windowP = windowPointer;
    this->config = config;
    this->rectangleCount = rCount;
    for (int i=0;i < rCount;++i)
        this->rectangles.push_back(*rectanglePointers[i]);
    this->textCount = tCount;
    for (int i=0; i<tCount;++i)
        this->texts.push_back(*textPointers[i]);
    
}

Stage::~Stage(){
    
}
