/*
  file: src/chess_game.cpp
  purpose: define methods of the ChessGame class
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#include "chess_game.h"

sf::Vector2i ChessGame::find_piece(char fen){
  // returns the first found piece's coordinates
  // or {-1, -1} if none were found
  for (auto piece : this->pieces){
    if (piece->fenchar == fen)
      return {piece->x, piece->y};
  }
  return {-1, -1};
}
    
void ChessGame::update_moves(bool simple){
  this->board->clear_attacks();
  for (auto piece : this->pieces){
    piece->update_moves(simple);
  }
}

void ChessGame::clear_last_moves(){
  for (auto piece : pieces){
    piece->last_move_distance = -1;
  }
}
void ChessGame::print_debug_info(){
  std::cout<<"-- Pieces --\n";
  for (auto piece : pieces)
    piece->print_debug_info();
}

void ChessGame::remove_piece(int x, int y){
  if (!this->board->board[x][y]) return;
  int remId = this->board->board[x][y]->id;
  for(int i=0; i<(int)this->pieces.size();++i){
    if (this->pieces[i]->id == remId){
      delete this->pieces[i];
      this->pieces[i] = this->pieces.back();
      this->pieces.pop_back();
      break;
    }    
  }
}

void ChessGame::remove_all_pieces(){
  this->board->remove_all_pieces();
  this->pieces.clear();
}

void ChessGame::add_piece(char fenchar, int x, int y){
  Piece* new_piece = new Piece(this->board, x, y, (int)pieces.size());
  new_piece->load_from_fen(fenchar, x, y);
  this->pieces.push_back(new_piece);
  //this->board->board[x][y]->print_debug_info();
  //getchar();
}

void ChessGame::load_from_fen(string fen){
  this->remove_all_pieces();
  int x=0, y=7;
  for (int i=0; i < (int)fen.length();++i){
    if (fen[i] == '/') {
      x=0; y--;
      continue;
    }
    if (fen[i] >= '0' && fen[i] <='9'){
      x += fen[i] - '0';
      continue;
    }
    if (fen[i] == ' ') {
      //this->print_debug_info();
      break;
    }
    if ((fen[i] >= 'a' && fen[i]<='z') ||
	(fen[i] >= 'A' && fen[i] <='Z')){
      this->add_piece(fen[i], x, y);
      x++;
    }
  }
}

string ChessGame::save_to_fen(){
  string fen = "";
  int empty = 0;
  for (int y=0;y<this->board->y;++y){
    for (int x=0;x<this->board->x;++x){
      if (this->board->board[x][this->board->y-1-y]){
	if(empty) {
	  fen+=std::to_string(empty);
	  empty = 0;
	}
	fen+=std::string(1,this->board->board[x][this->board->y-1-y]->fenchar);
      }
      else empty++;
    }
    if(empty) {
      fen+=std::to_string(empty);
      empty = 0;
    }
    fen+="/";
  }
  return fen;
}

void ChessGame::user_input(){
  char input = getchar();
  fflush(stdin);
  if (this->board->enumerator.find(input) != this->board->enumerator.end()){
    this->selectedPiece->move(this->board->enumerator[input]);
    this->selectedPiece = nullptr;
    this->board->clear_selection();
    this->board->clear_highlights();
    return;
  }
  if(input == ENTER && 
     this->board->selection_x+1 &&
     this->board->selection_y+1){
    this->selectedPiece = this->board->board
      [this->board->selection_x][this->board->selection_y];
    //this->selectedPiece->print_debug_info();
    //getchar();
    this->board->clear_selection();
    this->board->clear_highlights();
    return;
  }
  int a;
  
  if (input <= '8' && input >= '1'){
    this->selectedPiece = nullptr;
    a = input - '1';
    this->board->select(row, a);
    this->board->clear_highlights();
    this->board->enumerator.clear();
    return;
  }
  if (input <= 'h' && input >= 'a'){
    this->selectedPiece = nullptr;
    a = input - 'a';
    this->board->select(column, a);
    this->board->clear_highlights();
    this->board->enumerator.clear();
    return;
  }
  this->board->clear_selection();
}

ChessGame::ChessGame(Interface* interface){
  this->interface = interface;
  this->board = new Board(this,8,8);
  this->pieces.clear();
  this->selectedPiece = nullptr;
}

void ChessGame::loop(){
  if (this->interface)
    this->interface->canonical_mode(false);
  while (true){
    this->update_moves(true);
    if (this->selectedPiece)
      board->set_highlights(this->selectedPiece->moves);
    clear();
    this->board->print();
    user_input();
  }
}

ChessGame::~ChessGame(){
  delete this->board;
}
