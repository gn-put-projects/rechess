/*
  file: src/piece.h
  purpose: declare the piece class
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PIECE_H
#define PIECE_H

#include <ctime>
#include <cmath>
#include <string>
#include <vector>
#include <climits>
#include <map>

// Forward declaration

class Piece;

#include "helpers.h"
#include "board.h"
#include "position.h"
#include "chess_rules.h"
#include "movement.h"

using std::string;
using std::vector;

class Piece{
private:
  vector<vector<Special_move*> > special_move_used;
  void evolve();
public:
  int x;
  int y;
  int id;
  string name;
  int moves_made;
  string color;
  char fenchar;
  int last_move_distance;
  void print();
  void load_from_fen(char, int, int);
  Board* board;
  Piece(Board*, int, int, int);
  void move(Move);
  void move(Move*);
  void update_moves(bool);
  Movement movement;
  bool check_condition (Condition);
  vector<Special_move> special_moves;
  vector<Move> moves;
  void print_debug_info();
  void promote(char);
  vector<char> get_promotion_options();
  Move* get_move(Move);
};

#endif
