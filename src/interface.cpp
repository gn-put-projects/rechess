#include "interface.h"

void version_info(){
  std::cout<<"ReChess version "<<VERSION<<"\n";
  std::cout<<"Copyright Artur Gulik, Szymon Nowak "<<YEARS<<" (C)\n";
  printf("License: GNU GPL version 3 <https://www.gnu.org/licenses/gpl-3.0.html>\n");
  printf("This is free software: you are free to change and redistribute it.\n");
  printf("There is NO WARRANTY, to the extent prermitted by law.\n");
}

void help_info(){
  printf("Usage:\n");
  printf("\trechess [options]\n\n");
  
  printf("Options:\n");
  printf("\t-h, --help\t\tDisplay this help information and exit\n");
  printf("\t-v, --version\t\tDisplay version information and exit\n");
}
/**
  A function for setting the canonical mode on or off in the terminal
  @param {bool} canonical - Specifies whether to turn canonical mode on or off
  @returns void
**/
void Interface::canonical_mode(bool canonical){
  if(canonical){
    // Resetting termios to defaults
    tcsetattr(STDIN, TCSANOW, &(this->default_termios));
    return;
  }
  struct termios new_termios;
  tcgetattr(STDIN, &new_termios);
    
  new_termios.c_lflag &= ~ICANON;
  new_termios.c_cc[VMIN] = 1;
  new_termios.c_cc[VTIME] = 0;
  tcsetattr(0, TCSANOW, &new_termios);
}

Interface::Interface(){
  // Set default termios to go back to
  tcgetattr(STDIN, &(this->default_termios));
  
  this->canonical_mode(false);
}

Interface::~Interface(){
  this->canonical_mode(true);
}

void Interface::loop(){
  char input;
  while(0){
    clear();
    std::cout<<"ReChess\n";
    std::cout<<"1. Menu Item 1\n";
    std::cout<<"2. Menu Item 2\n";
    std::cout<<"3. Menu Item 3\n";
    std::cout<<"0. Exit the program\n";
    input = getchar();
    fflush(stdin);
    switch(input){
    case '1':
    case '2':
    case '3':
      clear();
	printf("Press any key to continue...");
	input = getchar();
	fflush(stdin);
	break;
    case '0':
      clear();
      printf("Are you sure you want to exit? [y/N]\n");
      input = getchar();
      fflush(stdin);
	if (input == 'y' || input == 'Y'){
	  clear();
	  return;
	}
	break;
    }
  }
}

int Interface::process_flags(int argc, char* argv[]){
  for (int i=1; i < argc; ++i){
    string arg(argv[i]);
    if (arg == "-h" || arg == "--help"){
      help_info();
      return 1;
    }
    if (arg == "-v" || arg == "--version"){
      version_info();
      return 1;
    }
    std::cout<<"Unrecognized option: "<<arg<<"\n";
    return 1;
  }
  return 0;
}
