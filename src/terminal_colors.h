/*
  file: src/terminal_colors.h
  purpose: define an enum to be used with escape sequences to set
           color for stdout (used in CLI)
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TERM_COLORS_H
#define TERM_COLORS_H

enum colors{black=30, red, green, yellow,
	    blue, magenta, cyan, white,
	    bright_black=90, bright_red, bright_green,
	    bright_yellow, bright_blue, bright_magenta,
	    bright_cyan, bright_white};
#endif
