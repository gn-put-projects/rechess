/*
  file: src/piece.cpp
  purpose: define methods for the piece class
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#include "piece.h"

Move* Piece::get_move(Move move){
  for (auto& m : this->moves){
    if (m.x == move.x && m.y == move.y)
      return &m;
  }
  return NULL;
}

bool Piece::check_condition(Condition condition){
  return this->board->check_condition(condition, this);
}

int distance(int x1, int y1, int x2, int y2){
  return std::max(std::abs(x1-x2), std::abs(y1-y2));
}

Piece::Piece(Board* board, int x, int y, int id){
  this->board = board;
  this->movement = chess_rules::king_movement;
  this->special_moves = {};
  this->name = "unset";
  this->color = "unset";
  this->fenchar = 'K';
  this->moves_made = 0;
  this->id = id;
  this->board->place(this, x, y);
}

void Piece::print(){
  std::map<char, string> piece_symbol = {
    { 'r', "♜" }, { 'R', "♖"},
    { 'n', "♞" }, { 'N', "♘"},
    { 'b', "♝" }, { 'B', "♗"},
    { 'q', "♛" }, { 'Q', "♕"},
    { 'k', "♚" }, { 'K', "♔"},
    { 'p', "♟" }, { 'P', "♙"}
  };
  std::cout<<piece_symbol.at(this->fenchar);
}

void Piece::promote(char fen){
  this->load_from_fen(fen, this->x, this->y);
}

vector<char> Piece::get_promotion_options(){
  if (!this->color.compare("white")){
    return {'N', 'B', 'R', 'Q'};
  }
  else return {'n', 'b', 'r', 'q'};
}

void Piece::print_debug_info(){
  std::cout<<"piece {\n";
  std::cout<<"  x: "<<this->x<<",\n";
  std::cout<<"  y: "<<this->y<<",\n";
  std::cout<<"  color: "<<this->color<<",\n";
  std::cout<<"  fenchar: "<<this->fenchar<<"\n";
  std::cout<<"  possible moves: "<<this->moves.size()<<"\n";
  std::cout<<"  moves made: "<<this->moves_made<<"\n";
  std::cout<<"}\n";
}

void Piece::load_from_fen(char fen, int x, int y){
  this->fenchar = fen;
  std::map<char, Movement> piece_movement = {
    { 'r', chess_rules::rook_movement },
    { 'n', chess_rules::knight_movement },
    { 'b', chess_rules::bishop_movement },
    { 'q', chess_rules::queen_movement },
    { 'k', chess_rules::king_movement },
    { 'p', chess_rules::pawn_movement }
  };
  std::map<char, string> piece_names = {
    { 'r', "rook" },
    { 'n', "knight" },
    { 'b', "bishop" },
    { 'q', "queen" },
    { 'k', "king" },
    { 'p', "pawn" }
  };
  this->color = "black";
  if (fen >= 'A' && fen <='Z') {
    this->color = "white";
    fen -= 'A'-'a';
  }
  this->name = piece_names[fen];
  this->board->place(this,x,y);
  this->movement = piece_movement[fen];

  this->special_moves.clear();
  
  if (fen == 'k'){
    this->special_moves = {
      chess_rules::castling_queenside,
      chess_rules::castling_kingside
    };
    return;
  }
  
  if (fen == 'p'){
    if (this->color == "white")
      this->special_moves = chess_rules::white_pawn;
    else
      this->special_moves = chess_rules::black_pawn;
  }
}

void Piece::evolve(){
  switch(this->fenchar){
  case 'p':
    this->promote('n');
    break;
  case 'P':
    this->promote('N');
    break;
  case 'n':
    this->promote('b');
    break;
  case 'N':
    this->promote('B');
    break;
  case 'b':
    this->promote('r');
    break;
  case 'B':
    this->promote('R');
    break;
  case 'r':
    this->promote('q');
    break;
  case 'R':
    this->promote('Q');
    break;
  }
}
void Piece::move(Move move){
  if (this->board->board[move.x][move.y])
    this->evolve();
  this->moves_made++;
  this->board->clear_last_moves();
  this->last_move_distance = distance
    (move.x, move.y, this->x, this->y);
  this->board->move(this, move.x, move.y);
}

void Piece::move(Move* move){
  //std::cout<<"Attempting move ("<<move->x<<";"<<move->y<<")\n";
  if (!move->special) {
    return this->move(*move);
  }
  bool e=false;
  if (this->board->board[move->x][move->y])
    e=true;
  for (auto action : move->special->actions){
    board->manage_action(action, this);
  }
  if (e) this->evolve();
}

void Piece::update_moves(bool simple){
  this->moves.clear();
  int moves_found = 0;
  // Testing movement in order:
  // left, right, forward, backward, topleft, topright
  // botleft, botright
  int bound[] = {
    this->movement.left,
    this->movement.right,
    this->movement.forward,
    this->movement.backward,
    this->movement.topleft,
    this->movement.topright,
    this->movement.botleft,
    this->movement.botright,
    this->movement.knight
  };
  
  int x_change[] = {-1,1,0, 0,-1,1,-1, 1};
  int y_change[] = { 0,0,1,-1, 1,1,-1,-1};

  int x,y;
  for (int dir=0;dir<8;++dir){
    for(int c=1;c<=bound[dir];++c){
      x = this->x + c*x_change[dir];
      y = this->y + c*y_change[dir];
      if (!this->board->isEmpty(x, y)){
	if (this->board->isEnemy(x, y, this))
	  c = INT_MAX-1;
	else break;
      }
      if (!simple){
	Move test = {x, y, nullptr};
	//std::cout<<"Before "<<this->board->board[this->x][this->y]<<"\n";
	if (!this->board->isLegal(this, &test)){
	  break;//continue;
	}
	if (!this->board->board[this->x][this->y])
	  std::cout<<"!"<<this->x<<" , "<<this->y<<"\n";
	//std::cout<<"After "<<this->board->board[this->x][this->y]<<"\n";
      }
      moves_found++;
      this->moves.push_back({x, y, nullptr});
      if (!this->color.compare("white"))
	this->board->attackedByW[x][y] = true;
      else
	this->board->attackedByB[x][y] = true;
    }
  }
  int knight = 8;
  int knight_x_change[] = {-2,-1,1,2, 2, 1,-1,-2};
  int knight_y_change[] = { 1, 2,2,1,-1,-2,-2,-1};
  
  for (int dir=0;dir<8;++dir){
    for(int c=1;c<=bound[knight];++c){
      x = this->x + c*knight_x_change[dir];
      y = this->y + c*knight_y_change[dir];
      if (!this->board->isEmpty(x,y)
	  && !this->board->isEnemy(x,y, this))
	break;
      if (!simple){
	Move test = {x, y, nullptr};
	//std::cout<<"Before "<<this->board->board[this->x][this->y]<<"\n";
	if (!this->board->isLegal(this, &test)){
	  break;//continue;
	}
	if (!this->board->board[this->x][this->y])
	  std::cout<<"!!"<<this->x<<" , "<<this->y<<"\n";
	//std::cout<<"After "<<this->board->board[this->x][this->y]<<"\n";
      }
      moves_found++;
      this->moves.push_back({x, y, nullptr});
      if (!this->color.compare("white"))
	this->board->attackedByW[x][y] = true;
      else
	this->board->attackedByB[x][y] = true;
    }
  }
  bool conditions_met;
  for (auto& move : this->special_moves){
    conditions_met = true;
    for (auto condition : move.conditions){
      if (!this->check_condition(condition)) {
	conditions_met = false;
	break;
      }
    }
    if (!conditions_met) continue;
    for (auto action : move.actions){
      if (!action.body.substr(0,4).compare("move")){
	get_two_ints_from_string(action.body.substr(5), &x, &y);
	x += this->x;
	y += this->y;
	break;
      }
    }
    if (!simple){
	Move test = {x, y, &move};
	//std::cout<<"Before "<<this->board->board[this->x][this->y]<<"\n";
	if (!this->board->isLegal(this, &test)){
	  break;//continue;
	}
	if (!this->board->board[this->x][this->y])
	  std::cout<<"!!!"<<this->x<<" , "<<this->y<<"\n";
	//std::cout<<"After "<<this->board->board[this->x][this->y]<<"\n";
      }
    if (x != -1){
      moves_found++;
      this->moves.push_back({x,y, &move});
      //this->board->attacked[x][y] = true;
    }
  }
}
