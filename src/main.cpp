#include "chess_game.h"
#include "interface.h"
#include "sfml_frontend/app.h"

int main(int argc, char* argv[]){
  Interface cli;
  if (cli.process_flags(argc, argv)) return 0;

  App app;
  app.loop();
    
  //ChessGame game(&cli);
  //game.load_from_fen("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR ");
  //game.load_from_fen("r3k2r/pppppppp/8/8/8/8/PPPPPPPP/R3K2R ");
  //game.loop();
  
  //cli.loop();
  
  return 0;
}
