/*
  file: src/board.cpp
  purpose: define methods for the board class
    
  This file is part of 'ReChess'.
  
  'ReChess' is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License,
  or (at your option) any later version.

  'ReChess' is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU General Public License for more details.

  You should have received a copy of the GNU General Public License along with
  'ReChess'. If not, see <https://www.gnu.org/licenses/>.
*/

#include "board.h"

const int square_w = 7;
const int square_h = 3;
const string pixel = "█";

bool Board::isLegal(Piece* piece, Move* move){
  int x = piece->x;
  int y = piece->y;
  Board* backup = new Board(*this);
  
  std::cout<<"Piece to test for legalness: "<<piece<<"\n";
  std::cout<<"Board where it should be: "<<this->board[x][y]<<"\n";
  string color = this->board[x][y]->color;

  this->board[x][y]->move(*move);
  this->game->update_moves(true);
  std::cout<<"Board after sym move: "<<this->board[x][y]<<"\n";
  char king;
  if (!color.compare("white")){
    king = 'K';
  }
  else king = 'k';
  auto [kx,ky] = this->game->find_piece(king);
  bool output;
  if (kx == -1) {
    output = true;
  }
  else if (!color.compare("white")){
    if (this->attackedByB[kx][ky]){
      output = false;
    }
    else output = true;
  }
  else if (this->attackedByW[kx][ky]){
    output = false;
  }
  else output = true;

  delete this;
  *this = *backup;
  std::cout<<"Board after undo: "<<this->board[x][y]<<"\n";
  return output;
}

string hline(int length){
  return repeat("─", length);
}

string space(int spaces){
  if (spaces < 0)
    throw std::invalid_argument("Number of spaces has to be at least 0");
  return string(spaces, ' ');
}

void Board::manage_action(Action action, Piece* piece){
  int x = action.x + piece->x;
  int y = action.y + piece->y;
  if (x<0 || y<0 || x>this->x-1 || y>this->y-1){
    throw std::invalid_argument
      ("Incorrect piece coordinates. Remember that moving a piece updates"
       " all relative coordinates for the following actions!");
  }
  string body = action.body;
  int dest_x, dest_y;
  if (body.substr(0,6) == "remove"){
    this->game->remove_piece(x,y);
    return;
  }
  if (body.substr(0,4) == "move"){
    get_two_ints_from_string(action.body.substr(5), &dest_x, &dest_y);
    int dx = dest_x + piece->x, dy = dest_y + piece->y;
    if (dx<0 || dy<0 || dx>this->x-1 || dy>this->y-1){
      throw std::invalid_argument
	("Incorrect destination coordinates. Remember that moving a piece"
	 " updates all relative coordinates for the following actions!");
    }
    this->board[x][y]->move({dx, dy});
  }
}
bool Board::check_condition(Condition condition, Piece* piece){
  int x = condition.x + piece->x;
  int y = condition.y + piece->y;
  string body = condition.body;
  if (body.substr(0,5) == "piece"){
    string piece_name = body.substr(6);
    return this->is_piece(x, y, piece_name);
  }
  if (body.substr(0,5) == "enemy"){
    return this->isEnemy(x, y, piece);
  }
  if (body.substr(0,6).compare("played") == 0){
    int actual = this->board[x][y]->moves_made;
    int needed = stoi(body.substr(9));
    if (body[7] == '>'){
      return (actual > needed);
    }
    if (body[7] == '<'){
      return (actual < needed);
    }
    if (body[7] == '='){
      return (actual == needed);
    }
    throw std::invalid_argument
      ("An operator expected in condition string, index 7"); 
  }
  if (body.substr(0,8).compare("attacked") == 0){
    bool attacked;
    if (!piece->color.compare("white")){
      attacked = this->attackedByB[x][y];
    }
    else attacked = this->attackedByW[x][y];
    if (body[9] == '0')
      return !attacked;
    return attacked;
  }
  if (body.substr(0,5).compare("moved") == 0){
    int actual = this->board[x][y]->last_move_distance;
    int needed = stoi(body.substr(8));
    
    if (body[6] == '>'){
      return (actual > needed);
    }
    if (body[6] == '<'){
      return (actual < needed);
    }
    if (body[6] == '='){
      return (actual == needed);
    }
    throw std::invalid_argument
      ("An operator expected in condition string, index 6"); 
  }
  throw std::invalid_argument
      ("Unrecognized condition to check"); 
}

void Board::clear_attacks(){
  this->attackedByW.assign(this->x, vector<bool>(this->y, false));
  this->attackedByB.assign(this->x, vector<bool>(this->y, false));
}

void Board::remove_all_pieces(){
  this->board =
    vector<vector<Piece*> >(this->x, vector<Piece*>(this->y,nullptr));
}

void Board::print(){
  int padding = 6;
  int coord_spacing = 2;
  bool highlight;
  int high_black = bright_red;
  int high_white = bright_red;
  bool middle; bool bottom;
  for (int y=(this->y)-1; y > -1; --y){
    for (int y_layer = 0; y_layer < square_h; ++y_layer){
      
      middle = (y_layer == square_h/2);
      bottom = (y_layer == square_h-1);
      if (middle){
	std::cout<<space(padding-int_length(y+1)-coord_spacing);
	std::cout<<y+1<<space(coord_spacing);
      }
      else std::cout<<space(padding);
      for (int x=0; x<this->x; ++x){
	bool x_was_selected = this->selection_x+1;
	bool y_was_selected = this->selection_y+1;
	/* A square should be highlighted if:
	   (
	   - no x is selected (whole column situation)
	     or selected x is equal to current x
	   and
	   - no y is selected (whole row situation)
	     or selected y is equal to current y
	   and
	   - either coordinate has been selected
	   )
	   or if highlighted vector indicates that it should be
	*/
	highlight = this->highlighted[x][y] ||
	  ((x == this->selection_x || !x_was_selected) &&
	   (y == this->selection_y || !y_was_selected) &&
	   (x_was_selected || y_was_selected));
	int color = ((x+y)%2) ? white : black;
	int color_inv = (color == white)? black : white;
	if (highlight){
	  color = (color==black)? high_black : high_white;
	}
	colors(color, color);
	if (middle && this->board[x][y]){
	  std::cout<<repeat(pixel, square_w/2);
	  colors(color_inv, color);
	  this->board[x][y]->print();
	  colors(color, color);
	  std::cout<<repeat(pixel, square_w/2);
	}
	else {
	  std::cout<<repeat(pixel, square_w-1);
	  if (bottom && this->enumerated[x][y] != '\0'){
	    colors(color_inv, color);
	    std::cout<<this->enumerated[x][y];
	    colors(color, color);
	  }
	  else std::cout<<pixel;
	}
      }
      restore_colors();
      std::cout<<"\n";
    }
  }
}
Board::Board(ChessGame* game, int x, int y){
  std::cout<<"Creating board\n";
  this->x = x;
  this->y = y;
  this->size = x*y;
  this->game = game;
  this->board = vector<vector<Piece*> > (x, vector<Piece*>(y,nullptr));
  this->highlighted = vector<vector<bool> >(x, vector<bool>(y, false));
  this->enumerated = vector<vector<char> >(x, vector<char>(y, '\0'));
  this->attackedByW = vector<vector<bool> >(x, vector<bool>(y, false));
  this->attackedByB = vector<vector<bool> >(x, vector<bool>(y, false));
  this->selection_x = -1;
  this->selection_y = -1;
}
/*
Board::Board(Board board){
  this->x = board.x;
  this->y = board.y;
  this->size = this->x * this->y;
  this->game = new Game(board.game);
}
*/
Board::~Board(){
  std::cout<<"deleting board\n";
  this->board.clear();
  this->highlighted.clear();
  this->enumerated.clear();
  this->attackedByW.clear();
  this->attackedByB.clear();
}

void Board::remove(Piece* piece){
  int x = piece->x; int y = piece->y;
  //if (x<0 || y<0 || x>this->x-1 || y>this->y-1)
  //return;
  this->board[x][y] = nullptr;
}

void Board::place(Piece* piece, int x, int y){
  this->board[x][y] = piece;
  piece->x = x; piece->y = y;
}

void Board::move(Piece* piece, int x, int y){
  this->game->remove_piece(x,y);
  this->board[piece->x][piece->y] = nullptr;
  this->board[x][y] = piece;
  piece->x = x; piece->y = y;
}

void Board::select(int row_or_column, int id){
  if (row_or_column){
    this->selection_x = id;
    return;
  }
  this->selection_y = id;
}

void Board::clear_selection(){
  this->selection_x = -1;
  this->selection_y = -1;
}

bool Board::isEmpty(int x, int y){
  if (x<0 || y<0 || x>this->x-1 || y>this->y-1)
    return false;
  if (this->board[x][y]) return false;
  return true;
}

bool Board::isEnemy(int x, int y, Piece* self){
  if (x<0 || y<0 || x>this->x-1 || y>this->y-1)
    return false;
  if (!this->board[x][y]) return false;
  return (bool)(this->board[x][y]->color.compare(self->color));
}

bool Board::is_piece(int x, int y, string name){
  if (x<0 || y<0 || x>this->x-1 || y>this->y-1)
    return false;
  if (!this->board[x][y])
    return (!name.compare("none"));
  if (!name.compare("any")) return true;
  return (!this->board[x][y]->name.compare(name));
}

void Board::clear_highlights(){
  this->highlighted.assign(x, vector<bool>(y, false));
  this->enumerated.assign(x, vector<char>(y, '\0'));
  this->enumerator.clear();
}

void Board::set_highlights(vector<Move>& moves){
  this->highlighted.assign(x, vector<bool>(y, false));
  this->enumerated.assign(x, vector<char>(y, '\0'));
  //first character that is not yet used
  char c = 'a' + this->x;
  for (auto& move : moves){
    this->highlighted[move.x][move.y] = true;
    this->enumerated[move.x][move.y] = c;
    this->enumerator[c] = &move;
    c++;
  }
}

void Board::clear_last_moves(){
  this->game->clear_last_moves();
}
