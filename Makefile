MAJOR=0
MINOR=1
VERSION=$(MAJOR).$(MINOR)

#-----------------------------------
# Paths for source and object files
S=src
SF=$S/sfml_frontend
O=$S/obj

#---------------
# Compiler and compiler flags
CC=g++
CFLAGS=-Wall -g
OBJ_CFLAGS=-c $(CFLAGS)

LIBSFML=-lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lsfml-network

.PHONY: test compile clean

test: rechess
	@./rechess

compile: rechess

rechess:  $O/sfml_chess_game.o $O/stage.o $O/app.o $O/chess_game.o $O/piece.o $O/helpers.o $O/board.o $O/interface.o $O/main.o
	$(CC) $(CFLAGS) $^ -o $@ $(LIBSFML)

$O/main.o: $S/main.cpp
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/interface.o: $S/interface.cpp $S/interface.h 
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/board.o: $S/board.cpp $S/board.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/helpers.o: $S/helpers.cpp $S/helpers.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/piece.o: $S/piece.cpp $S/piece.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/chess_game.o: $S/chess_game.cpp $S/chess_game.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/app.o: $(SF)/app.cpp $(SF)/app.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/stage.o: $(SF)/stage.cpp $(SF)/stage.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/sfml_chess_game.o: $(SF)/sfml_chess_game.cpp $(SF)/sfml_chess_game.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

$O/renderable.o: $(SF)/renderable.cpp $(SF)/renderable.h
	$(CC) $(OBJ_CFLAGS) $< -o $@

clean:
	rm -f rechess $O/*.o
